
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;

public class Application {

	public static void main(String[] args) {
		tryCommonsIO();
		tryCommonsLang();
	}

	private static void tryCommonsIO() {
		try {
			final String path = FilenameUtils.getPath(".");
			System.out.println("If you are here, apache commons-io was loaded up! " + path);
		} catch (Throwable t) {
			final String message = String.format("LOAD ERROR {%s}: %s", t.getClass().getSimpleName(), t.getMessage());
			System.out.println(message);
		}
	}

	private static void tryCommonsLang() {
		try {
			final boolean found = StringUtils.contains("Hello World!", "lo Wo");
			System.out.println("If you are here, apache common-lang lib was loaded up!");
		} catch (Throwable t) {
			final String message = String.format("LOAD ERROR {%s}: %s", t.getClass().getSimpleName(), t.getMessage());
			System.out.println(message);
		}
	}
}
